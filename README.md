
**Cutting Tree**
----------

 Given a binary tree with identifiers nodes from 1 to **N**. 1 - the root of the tree. Each tree element is assigned a weight **W.** And **K** is the maximum possible number of delete operations.
 It is necessary to implement the method of removing a subtree from a tree.
```def removeSubtree (n: Node): Unit```, where n is the subtree root to be deleted
Applying this method no more than **K** times, you need to get a tree with the maximum possible weight. The tree should be generated automatically, randomly with parameters within the specified values.
The output of the initial tree, the weight of the final tree and the number of remote subtrees must be output.

#### Input parametrs:

- **1 <K <100 - delete operations**
- **100 <= N <= 1000 (that is, the tree contains up to 1000 nodes)**
- -**1000 <= W <= 1000 - the weight of the tree node**

----------

For running app in application dir use

```
sbt run
```

For running tests

```
sbt test
```




























































