package com.tree.binary

import scala.annotation.tailrec

sealed trait Tree[+T] {

  def orderedNodes: List[Tree[T]] = this match {
    case node: Node[T] => node.leftNode.orderedNodes ::: List(node) ::: node.rightNode.orderedNodes
    case Empty         => List()
  }

  def orderedNodesWithoutRoot: List[Tree[T]] = this match {
    case node: Node[T] => node.leftNode.orderedNodes ::: node.rightNode.orderedNodes
    case Empty         => List()
  }

  @tailrec
  private def fold[A >: T, B](trees: List[Tree[A]], acc: B)(f: (B, A) => B): B = trees match {
    case Nil                            => acc
    case Empty :: t                     => fold(t, acc)(f)
    case Node(Empty, value, Empty) :: t => fold(t, f(acc, value))(f)
    case Node(left, value, right) :: t  => fold(left :: right :: t, f(acc, value))(f)
  }

  def map[A](f: T => A): Tree[A] = this match {
    case Node(leftNode, nodeValue, rightNode) => Node(leftNode.map(f), f(nodeValue), rightNode.map(f))
    case Empty                                => Empty
  }

  def cut[A >: T](tree: Tree[A])(implicit equality: TreeEquality[A]): Tree[A] = this match {
    case node: Node[T] if equality.eq(node, tree)           => Empty
    case node: Node[T] if equality.eq(node.leftNode, tree)  => node.copy(leftNode = Empty)
    case node: Node[T] if equality.eq(node.rightNode, tree) => node.copy(rightNode = Empty)
    case Node(leftNode, nodeValue, rightNode)               => Node(leftNode.cut(tree), nodeValue, rightNode.cut(tree))
    case _                                                  => Empty
  }

  def right: Tree[T] = this match {
    case Node(_, _, rightNode) => rightNode
    case _                     => Empty
  }

  def left: Tree[T] = this match {
    case Node(leftNode, _, _) => leftNode
    case _                    => Empty
  }

  def value: Option[T] = this match {
    case Node(_, value, _) => Some(value)
    case Empty             => None
  }

  def addValue[A >: T](v: A)(implicit ordering: Ordering[A]): Tree[A] = this match {
    case Node(leftNode, value, rightNode) =>
      if (ordering.gt(value, v)) Node(leftNode.addValue(v), value, rightNode)
      else Node(leftNode, value, rightNode.addValue(v))
    case Empty => Node(Empty, v, Empty)
  }

  def isLeaf: Boolean = this match {
    case Node(Empty, _, Empty) => true
    case _                     => false
  }

  def isEmpty: Boolean

  def weight[A >: T](implicit weight: WithWeight[A]): Int = fold(List(this), 0) { (acc, value) =>
    acc + weight(value)
  }

  override def toString: String = this match {
    case Empty         => ""
    case Node(leftNode, nodeValue, rightNode) => s"[$leftNode ${nodeValue} $rightNode]"
  }

}
case class Node[T](leftNode: Tree[T], nodeValue: T, rightNode: Tree[T]) extends Tree[T] {

  override def isEmpty: Boolean = false

}

case object Empty extends Tree[Nothing] {

  override def isEmpty: Boolean = true

}

object Tree {

  def apply[T](value: T): Tree[T] = Node(Empty, value, Empty)

}
