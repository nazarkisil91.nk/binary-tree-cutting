package com.tree.binary

import scala.language.higherKinds

object TreeDraw {

  val MaxValueLength = 3 //todo compute max length

  def draw[T](tree: Tree[T], maxValueLength: Int = MaxValueLength): Unit = {

    val deep        = treeDeep(tree)
    val maxX        = Math.pow(2, deep)
    val treeDrawMap = toTreeDrawMap(tree, deep)
    for (layer <- 0 until (deep * 2)) {
      val line = (0 until maxX.toInt).map(x => treeDrawMap.getOrElse((x, layer), EmptyElement).toLength(maxValueLength))
      println(line.mkString(""))
    }

  }

  private def treeDeep[T](tree: Tree[T], currentLayer: Int = 0): Int = {
    tree match {
      case Node(left, _, right) =>
        Math.max(treeDeep(left, currentLayer + 1), treeDeep(right, currentLayer + 1))
      case Empty => currentLayer
    }
  }

  private def toTreeDrawMap[T](tree: Tree[T], treeDeep: Int): Map[(Int, Int), TreeDrawMapElement[T]] = {

    val startLayer  = 0
    val startXPoint = Math.pow(2, treeDeep - 1).toInt - 1

    def toMap(tree: Tree[T],
              treeDrawMap: Map[(Int, Int), TreeDrawMapElement[T]],
              layer: Int,
              x: Int): Map[(Int, Int), TreeDrawMapElement[T]] = {
      tree match {
        case Node(left, _, right) =>
          val nextLayerShift = Math.pow(2, treeDeep - layer / 2 - 2).toInt

          val leftNodeMap = if (left.isEmpty) {
            treeDrawMap
          } else {
            val way   = for (i <- (x - nextLayerShift) to (x - 2)) yield ((i, layer + 1), Underscore)
            val arrow = ((x - 1, layer + 1), LeftArrow)
            val node  = ((x - nextLayerShift, layer + 2), Value(left.value.get))
            toMap(left, treeDrawMap.+(node, arrow).++(way), layer + 2, x - nextLayerShift)
          }

          if (right.isEmpty) {
            leftNodeMap
          } else {
            val way   = for (i <- x + 2 to x + nextLayerShift) yield ((i, layer + 1), Underscore)
            val arrow = ((x + 1, layer + 1), RightArrow)
            val node  = ((x + nextLayerShift, layer + 2), Value(right.value.get))
            toMap(right, leftNodeMap.+(node, arrow).++(way), layer + 2, x + nextLayerShift)
          }

        case Empty => treeDrawMap
      }

    }

    if (tree.isEmpty) {
      Map.empty[(Int, Int), TreeDrawMapElement[T]]
    } else {
      toMap(tree, Map((startXPoint, startLayer) -> Value(tree.value.get)), startLayer, startXPoint)
    }
  }

}

sealed trait TreeDrawMapElement[+T] {
  def toLength(length: Int): String
}

case class Value[T](value: T) extends TreeDrawMapElement[T] {
  override def toString: String = value.toString

  override def toLength(length: Int): String =
    if (value.toString.length < length) {
      value.toString + " " * (length - value.toString.length)
    } else {
      value.toString.take(length)
    }
}

case object LeftArrow extends TreeDrawMapElement[Nothing] {
  override def toString: String = "/"

  override def toLength(length: Int): String = "_" * (length - 1) + "/"
}

case object RightArrow extends TreeDrawMapElement[Nothing] {
  override def toString: String = "\\"

  override def toLength(length: Int): String = "\\" + "_" * (length - 1)
}

case object Underscore extends TreeDrawMapElement[Nothing] {
  override def toString: String = "_"

  override def toLength(length: Int): String = "_" * length
}

case object EmptyElement extends TreeDrawMapElement[Nothing] {
  override def toLength(length: Int): String = " " * length
}

trait TreeDrawer {

  implicit def treedDrawerExtension[T](tree: Tree[T]): TreedDrawerExtension[T] = new TreedDrawerExtension(tree)

}

final class TreedDrawerExtension[T](val tree: Tree[T]) extends AnyVal {

  def draw: Unit = TreeDraw.draw(tree)

  def drawWithLevel(level: Int): Unit = TreeDraw.draw(tree, level)

}
