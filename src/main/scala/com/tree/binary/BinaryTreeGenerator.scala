package com.tree.binary

import java.security.SecureRandom

import scala.annotation.tailrec

/**
  * Created by nk91 on 20.04.17.
  */
object BinaryTreeGenerator {

  def nodeGen[T](gen: Gen[T]): Gen[Tree[T]] =
    for {
      isEmpty <- Gen.booleans
      node    <- if (isEmpty) emptyNodeGen else nonEmptyNodeGen(gen)
    } yield node

  def nonEmptyNodeGen[T](gen: Gen[T]): Gen[Node[T]] =
    for {
      value <- gen
    } yield Node(Empty, value, Empty)

  def emptyNodeGen[T]: Gen[Tree[T]] = sample(Empty)

  def sample[T](node: Tree[T]): Gen[Tree[T]] = new Gen[Tree[T]] {
    override def generate: Tree[T] = node
  }

  def tree[T](gen: Gen[T], size: Int): Tree[T] = treeGen(gen, size).generate

  def treeGen[T](gen: Gen[T], size: Int): Gen[Tree[T]] =
    for {
      nodes <- listOfN(gen, size)
    } yield tree(nodes)

  def listOfN[T](gen: Gen[T], size: Int): Gen[List[T]] = new Gen[List[T]] {
    override def generate: List[T] = List.fill[T](size)(gen.generate)
  }

  def tree[T](values: Seq[T]): Tree[T] = values match {
    case head :: tail =>
      tail.foldLeft[Tree[T]](Tree(head)) { (acc: Tree[T], v: T) =>
        acc.addValue(v)(randomOrder)
      }
    case Nil => Empty
  }

  def randomOrder[T]: Ordering[T] = (_, _) => Gen.integers.generate

}

trait Gen[+T] { self =>

  def generate: T

  def map[S](f: T => S): Gen[S] = new Gen[S] {
    def generate: S = f(self.generate)
  }

  def flatMap[S](f: T => Gen[S]): Gen[S] = new Gen[S] {
    def generate: S = f(self.generate).generate
  }

  def single[S](x: S): Gen[S] = new Gen[S] {
    def generate: S = x
  }

  def suchThat(f: T => Boolean) = new Gen[T] {

    override def generate: T = genWhile(f, Gen.this.generate)

  }

  private def genWhile[A >: T](f: A => Boolean, value: A): A = if (f(value)) value else genWhile(f, Gen.this.generate)

}

object Gen {

  val integers = new Gen[Int] {
    val rand = new SecureRandom()

    def generate: Int = rand.nextInt()
  }

  val booleans: Gen[Boolean] = for {
    value <- integers
  } yield value > 0

  def oneOf[T](seq: Seq[T]): Gen[T] =
    for (index <- choose(0, seq.length)) yield seq(index)

  /**
    * Choose in range [min, max)
    *
    * @param min
    * @param max
    * @return
    */
  def choose(min: Int, max: Int): Gen[Int] = {
    if (min > max)
      throw new IllegalArgumentException(s"Lower bound is more then upper bound. ${max} must be more than ${min}")
    for (x <- Gen.integers) yield min + Math.abs(x % (max - min))
  }

}
