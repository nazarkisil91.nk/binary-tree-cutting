package com.tree.binary

/**
  * Created by nk91 on 18.04.17.
  */
object WithWeight extends WithWeightImplicits

trait WithWeight[T] {

  def apply(value: T): Int

}

trait WithWeightImplicits {

  implicit val IntegerWithWeight: WithWeight[Int] = (value: Int) => value

  implicit val indexWithWeight: WithWeight[(Int, Int)] = indexAndWeight => indexAndWeight._2

  implicit val integerTreeOrdering: Ordering[Tree[Int]] = (x: Tree[Int], y: Tree[Int]) => Integer.compare(x.weight, y.weight)

  implicit val tupledTreeOrdering: Ordering[Tree[(Int, Int)]] = (x: Tree[(Int, Int)], y: Tree[(Int, Int)]) =>
    Integer.compare(x.weight, y.weight)

}
