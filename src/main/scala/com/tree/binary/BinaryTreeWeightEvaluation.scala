package com.tree.binary

import scala.annotation.tailrec
import scala.language.higherKinds

/**
  * Created by nk91 on 20.04.17.
  */
object BinaryTreeWeightEvaluation extends BinaryTreeWeightEvaluation {

  def subTreesWeight[T](list: List[Tree[T]])(implicit withWeight: WithWeight[T]): Int =
    list.foldLeft(0)((a: Int, v: Tree[T]) => a + v.weight)

  def minWeightSubTrees[T](node: Tree[T], cuts: Int)(implicit withWeight: WithWeight[T],
                                                     equality: TreeEquality[T]): Evaluation[List[Tree[T]]] =
    if (cutsValidation(cuts)) Left(InvalidInputCuts)
    else Right(findMinWeightSubTrees(node, possibleCuts = cuts))

  private def cutsValidation[T](cuts: Int) = cuts < 0 || cuts > MaxPossibleCuts

  def analyze[T](tree: Tree[T], possibleCuts: Int)(implicit withWeight: WithWeight[T],
                                                   equality: TreeEquality[T]): Evaluation[List[Tree[T]]] =
    if (cutsValidation(possibleCuts)) Left(InvalidInputCuts)
    else Right(minWeightSubTrees(findMinWeightSubTrees(tree, possibleCuts), possibleCuts))

  private def minWeightSubTrees[T](subTrees: List[Tree[T]], possibleCuts: Int)(implicit withWeight: WithWeight[T],
                                                                               equality: TreeEquality[T]): List[Tree[T]] = {
    val minWeightTrees = subTrees
    // remainder union excludedTree == subTrees
    foldExclude(minWeightTrees)(subTrees) { (remainder: List[Tree[T]], excludedTree: Tree[T], currentMinWeightSubTrees: List[Tree[T]]) =>
      val c = selectMinN(findMinWeightSubTrees(excludedTree, possibleCuts) ++ remainder, possibleCuts)
      if (subTreesWeight(c) < subTreesWeight(currentMinWeightSubTrees)) minWeightSubTrees(c, possibleCuts) else currentMinWeightSubTrees
    }
  }

  private def selectMinN[T](list: List[Tree[T]], size: Int)(implicit withWeight: WithWeight[T]): List[Tree[T]] = {
    list.sortBy(_.weight).take(size)
  }

  @tailrec
  private def findMinWeightSubTrees[T](node: Tree[T], possibleCuts: Int, iteration: Int = 0, acc: List[Tree[T]] = List())(
      implicit withWeight: WithWeight[T],
      equality: TreeEquality[T]): List[Tree[T]] =
    if (iteration < possibleCuts)
      findMinWeightTree(node.orderedNodesWithoutRoot) match {
        case None          => acc
        case Some(minTree) => findMinWeightSubTrees(node.cut(minTree), possibleCuts, iteration + 1, acc :+ minTree)
      } else acc

  def findMinWeightTree[T](trees: List[Tree[T]])(implicit withWeight: WithWeight[T]): Option[Tree[T]] = {
    trees.reduceOption[Tree[T]]((t1, t2) => if (t1.weight > t2.weight) t2 else t1).filter(_.weight < 0)
  }

  private def foldExclude[T, B](acc: B)(list: List[T])(f: (List[T], T, B) => B): B = list match {
    case head :: tail => foldLoop(acc)(List.empty[T], head, tail)(f)
    case Nil          => acc
  }

  @tailrec
  private def foldLoop[T, B](acc: B)(pre: List[T], v: T, after: List[T])(f: (List[T], T, B) => B): B = after match {
    case Nil          => f(pre ++ after, v, acc)
    case head :: tail => foldLoop(f(pre ++ after, v, acc))(pre :+ v, head, tail)(f)
  }

}

trait BinaryTreeWeightEvaluation {

  val MaxPossibleCuts = 100

  type Evaluation[T] = Either[EvaluationError, T]

  implicit def binaryTreeWeightEvaluationExtension[T](tree: Tree[T]): BinaryTreeWeightEvaluationExtension[T] =
    new BinaryTreeWeightEvaluationExtension(tree)

  implicit def binaryTreesWeightExtension[T](trees: List[Tree[T]]): BinaryTreesWeightExtension[T] = new BinaryTreesWeightExtension(trees)

}

final class BinaryTreeWeightEvaluationExtension[T](val tree: Tree[T]) extends AnyVal {

  def analyze(cuts: Int)(implicit withWeight: WithWeight[T], equality: TreeEquality[T]): Evaluation[List[Tree[T]]] =
    BinaryTreeWeightEvaluation.analyze(tree, cuts)

}

final class BinaryTreesWeightExtension[T](val trees: List[Tree[T]]) extends AnyVal {

  def totalWeight(implicit withWeight: WithWeight[T]): Int = BinaryTreeWeightEvaluation.subTreesWeight(trees)

}

sealed abstract class EvaluationError(message: String)
case object InvalidInputCuts extends EvaluationError("Cuts must be in range [0, 100]")
