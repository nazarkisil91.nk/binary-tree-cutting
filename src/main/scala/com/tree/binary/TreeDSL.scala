package com.tree.binary

/**
  * Created by nk91 on 20.04.17.
  */
object TreeDSL extends TreeDSLExtensions

trait TreeDSLExtensions {

  implicit def nodeWrapper[T](value: Node[T]): NodeWrapper[T] = new NodeWrapper(value)

  def N[T](value: T): Node[T] = Node(Empty, value, Empty)

}

final class NodeWrapper[T](val value: Node[T]) extends AnyVal {

  def -->(node: Node[T]): Node[T] = node.copy(leftNode = value)
  def <--(node: Node[T]): Node[T] = value.copy(rightNode = node)

}