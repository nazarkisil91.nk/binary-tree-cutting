package com.tree.binary

/**
  * Created by nk91 on 18.04.17.
  */
object CuttingBinaryTreeApp extends App {

  type IndexWithWeight = (Int, Int)

  val possibleCuts     = 100
  val treesNodes       = 100
  val integerGenerator = Gen.choose(-1000, 1001)
  val tree             = BinaryTreeGenerator.treeGen(integerGenerator, treesNodes).generate
  tree.drawWithLevel(8)
  val result = for {
    result <- tree.analyze(possibleCuts)
  } yield {
    val startWeight = tree.weight
    val completeCuts = result.length
    val resultWeight = startWeight - result.totalWeight
    s"""
         |Start weight  $startWeight
         |Input cuts    $possibleCuts
         |Made cuts     $completeCuts
         |Result weight $resultWeight
     """.stripMargin
  }
  result match {
    case Right(message) => println(message)
    case Left(message)  => println(message)
  }

}
