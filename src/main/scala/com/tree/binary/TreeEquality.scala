package com.tree.binary

import scala.language.higherKinds
import scala.language.implicitConversions

/**
  * Created by kisilnazar on 23.04.17.
  */
trait TreeEquality[T] {

  def eq(v1: Tree[T], v2: Tree[T]): Boolean

}

trait ImplicitEquality {

  implicit val indexedTreesEquality: TreeEquality[(Int, Int)] = (v1: Tree[(Int, Int)], v2: Tree[(Int, Int)]) => v1 == v2

  implicit val treesEquality: TreeEquality[Int] = (v1: Tree[Int], v2: Tree[Int]) => v1.eq(v2)

}
