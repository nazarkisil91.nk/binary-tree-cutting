package com.tree.binary

import org.scalatest.{Matchers, WordSpec}

class TreeSpec extends WordSpec with Matchers {

  "Binary Tree" should {

    "is a leaf when left and right is Empty" in {
      val leaf = Node(Empty, "value", Empty)
      leaf.isLeaf shouldBe true
    }

    "orderedNodes should return all possible trees include himself" in {
      val tree: Tree[Int] = (N(1) --> N(2) <-- N(3)) --> N(4) <-- (N(5) --> N(6) <-- N(7))
      val subTrees        = tree.orderedNodes
      subTrees.length shouldBe 7
      subTrees.contains(tree) shouldBe true
    }

    "orderedNodesWithoutRoot  should return all possible trees exclude himself" in {
      val tree: Tree[Int] = (N(1) --> N(2) <-- N(3)) --> N(4) <-- (N(5) --> N(6) <-- N(7))
      val subTrees        = tree.orderedNodesWithoutRoot
      subTrees.length shouldBe 6
      subTrees.contains(tree) shouldBe false
    }

    "right must return right subtree" in {
      val rightSubTree = N(0) --> N(1) <-- N(2)
      val tree         = N(3) --> N(4) <-- rightSubTree
      tree.right shouldBe rightSubTree
    }

    "left must return left subtree" in {
      val leftSubTree = N(0) --> N(1) <-- N(2)
      val tree        = leftSubTree --> N(4) <-- N(3)
      tree.left shouldBe leftSubTree
    }

    "map function" in {
      val tree       = N(1) --> N(2) <-- N(3)
      val resultTree = tree.map(_ + 1)
      resultTree shouldBe N(2) --> N(3) <-- N(4)
    }

    "value must return Some if value exist other None" in {
      val existValue = N(10)
      existValue.value shouldBe Some(10)
      val notExistValue: Tree[Int] = Empty
      notExistValue.value shouldBe None
    }

    "weight of Empty tree is 0" in {
      val emptyTree: Tree[Int] = Empty
      emptyTree.weight shouldBe 0
    }

    "weight for Tree of integers must return sum of all" in {
      val tree = (N(1) --> N(2) <-- N(3)) --> N(4) <-- (N(5) --> N(6) <-- N(7))
      tree.weight shouldBe 28
    }

    "weight for tuple return sum of seconds values according to implicit WithWeight[(Int, Int)]" in {
      val tree = (N(0, 1) --> N(1, 2) <-- N(2, 3)) --> N(3, 4) <-- (N(4, 5) --> N(5, 6) <-- N(6, 7))
      tree.weight shouldBe 28
    }

    "addValue must input leaf according to Ordering" in {
      val tree = N(0) --> N(1) <-- N(3)
      tree.addValue(4) shouldBe N(0) --> N(1) <-- (N(3) <-- N(4))
      tree.addValue(-1) shouldBe (N(-1) --> N(0)) --> N(1) <-- N(3)
      tree.addValue(2) shouldBe N(0) --> N(1) <-- (N(2) --> N(3))
    }

    "cut subtree" in {
      val subTree = N(0) --> N(-1) <-- N(4)
      val tree = subTree --> N(1) <-- N(3)
      tree.cut(subTree) shouldBe N(1) <-- N(3)
    }

  }

}
