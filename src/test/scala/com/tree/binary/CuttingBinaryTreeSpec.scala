package com.tree.binary

import org.scalatest.{EitherValues, Matchers, WordSpec}

/**
  * Created by nk91 on 20.04.17.
  */
class CuttingBinaryTreeSpec extends WordSpec with Matchers with EitherValues {

  "Evaluating cut SubTrees for max Tree weight" should {

    "cuts mus be in range [0, 100]" in {
      BinaryTreeWeightEvaluation.analyze[Int](Empty, -1).isLeft shouldBe true
      BinaryTreeWeightEvaluation.analyze[Int](Empty, 0).isRight shouldBe true
      BinaryTreeWeightEvaluation.analyze[Int](Empty, 100).isRight shouldBe true
      BinaryTreeWeightEvaluation.analyze[Int](Empty, 101).isLeft shouldBe true
    }

    "weight of subtrees" in {
      BinaryTreeWeightEvaluation.subTreesWeight[Int](List()) shouldBe 0
      BinaryTreeWeightEvaluation.subTreesWeight[Int](List(Empty)) shouldBe 0
      BinaryTreeWeightEvaluation.subTreesWeight(List(N(1), N(2), N(3))) shouldBe 6
      BinaryTreeWeightEvaluation.subTreesWeight(BinaryTreesExample.tree.orderedNodes) shouldBe 1
    }

    "for Empty node must be zero" in {
      BinaryTreeWeightEvaluation.analyze[Int](Empty, 1).right.value.length shouldBe 0
      BinaryTreeWeightEvaluation.analyze[Int](Empty, 2).right.value.length shouldBe 0
      BinaryTreeWeightEvaluation.analyze[Int](Empty, 3).right.value.length shouldBe 0
      BinaryTreeWeightEvaluation.analyze[Int](Empty, 0).right.value.length shouldBe 0
    }

    "for test case 1" in {
      BinaryTreeWeightEvaluation.analyze(BinaryTreesExample.tree, 0).right.value.length shouldBe 0
      BinaryTreeWeightEvaluation.analyze(BinaryTreesExample.tree, 1).right.value.length shouldBe 1
      BinaryTreeWeightEvaluation.analyze(BinaryTreesExample.tree, 2).right.value.length shouldBe 2
      BinaryTreeWeightEvaluation.analyze(BinaryTreesExample.tree, 3).right.value.length shouldBe 3
      BinaryTreeWeightEvaluation.analyze(BinaryTreesExample.tree, 4).right.value.length shouldBe 3
    }

    "fot test tree2" in {
      BinaryTreesExample.expectedResultForTree2 forall {
        case (cuts, expectedCutWeight) =>
          val trees = BinaryTreeWeightEvaluation.analyze(BinaryTreesExample.tree2, cuts).right.value
          BinaryTreeWeightEvaluation.subTreesWeight(trees) === expectedCutWeight
      }
    }

    "fot test tree3" in {
      BinaryTreesExample.testCasesForTree3 forall {
        case (cuts, expectedCutWeight) =>
          val trees = BinaryTreeWeightEvaluation.analyze(BinaryTreesExample.tree3, cuts).right.value
          BinaryTreeWeightEvaluation.subTreesWeight(trees) === expectedCutWeight
      }
    }

    "remove two min leaf when weight large than 0" in {
      val tree             = N(-3) --> N(8) <-- N(-4)
      val resultForTwoCuts = BinaryTreeWeightEvaluation.analyze(tree, 2).right.value
      resultForTwoCuts.length shouldBe 2
      BinaryTreeWeightEvaluation.subTreesWeight(resultForTwoCuts) shouldBe -7
      val oneCuts = BinaryTreeWeightEvaluation.analyze(tree, 1).right.value
      oneCuts.length shouldBe 1
      BinaryTreeWeightEvaluation.subTreesWeight(oneCuts) shouldBe -4
    }

    "remove two min leaf when weight of three is less than 0" in {
      val tree             = N(-2) --> N(1) <-- N(-5)
      val resultForTwoCuts = BinaryTreeWeightEvaluation.analyze(tree, 2).right.value
      resultForTwoCuts.length shouldBe 2
      BinaryTreeWeightEvaluation.subTreesWeight(resultForTwoCuts) shouldBe -7
      val oneCuts = BinaryTreeWeightEvaluation.analyze(tree, 1).right.value
      oneCuts.length shouldBe 1
      BinaryTreeWeightEvaluation.subTreesWeight(oneCuts) shouldBe -5
    }

    "remove two min leaf when weight is less than 0 case 2" in {
      val tree = (N(-2) --> N(1) <-- N(-5)) --> N(0)
      val min  = BinaryTreeWeightEvaluation.minWeightSubTrees(tree, 3).right.value
      min.length shouldBe 1
      BinaryTreeWeightEvaluation.subTreesWeight(min) shouldBe -6
      val oneCuts = BinaryTreeWeightEvaluation.analyze(tree, 1).right.value
      oneCuts.length shouldBe 1
      BinaryTreeWeightEvaluation.subTreesWeight(oneCuts) shouldBe -6
      val misSubTrees = BinaryTreeWeightEvaluation.minWeightSubTrees(tree, 2).right.value
      misSubTrees.length shouldBe 1
      val resultForTwoCuts = BinaryTreeWeightEvaluation.analyze(tree, 2).right.value
      println(resultForTwoCuts)
      resultForTwoCuts.length shouldBe 2
      BinaryTreeWeightEvaluation.subTreesWeight(resultForTwoCuts) shouldBe -7
    }

    "when all trees has nodes with weight less than 0" in {
      val tree          = N(-2) --> N(-1000) <-- N(-5)
      val oneCutsResult = BinaryTreeWeightEvaluation.analyze(tree, 1).right.value
      println(oneCutsResult)
      oneCutsResult.length shouldBe 1
      BinaryTreeWeightEvaluation.subTreesWeight(oneCutsResult) shouldBe -5
      val twoCutsResult = BinaryTreeWeightEvaluation.analyze(tree, 2).right.value
      twoCutsResult.length shouldBe 2
      BinaryTreeWeightEvaluation.subTreesWeight(twoCutsResult) shouldBe -7
    }

    "do not remove root nodes whe it has weight less than zero" in {
      val tree = N(-1000)
      BinaryTreeWeightEvaluation.analyze(tree, 1).right.value.length shouldBe 0
      BinaryTreeWeightEvaluation.analyze(tree, 2).right.value.length shouldBe 0
      BinaryTreeWeightEvaluation.analyze(tree, 3).right.value.length shouldBe 0
    }

    "for case when sum of two subtrees are less than minimum subtree" in {
      // format: off
      val tree3: Tree[(Int, Int)] =
        (
          (
            N(1, -2)
                    -->
                       N(2, 1)
                    <--
            N(3, -5)
          )
                              -->
                                 N(4, 2)
                              <--
          (
            N(5, 1)
                   -->
                      N(6, 4)
                   <--
            N(7, 3)
          )
        )
      // format: on
      BinaryTreeWeightEvaluation.analyze(tree3, 0).right.value.length shouldBe 0
      val oneCut = BinaryTreeWeightEvaluation.analyze(tree3, 1).right.value
      // Should cut (-2,1,-5)
      oneCut.length shouldBe 1
      oneCut.head.weight shouldBe -6
      val twoCuts = BinaryTreeWeightEvaluation.analyze(tree3, 2).right.value
      // Should cut -2 and -5
      twoCuts.length shouldBe 2
      twoCuts.head.weight + twoCuts(1).weight shouldBe -7
    }

    "for any trees with weight more than zero always should be 0 cuts" in {
      val integerGenerator = Gen.choose(0, 100)
      val treesNodes       = 100
      val tree             = BinaryTreeGenerator.treeGen(integerGenerator, treesNodes).generate
      tree.analyze(1).right.value.length shouldBe 0
      tree.analyze(2).right.value.length shouldBe 0
      tree.analyze(3).right.value.length shouldBe 0
      tree.analyze(4).right.value.length shouldBe 0
    }

    """for any trees with weight less than zero and with left and right nodes
       always should be two cuts left and right for 2 and more cuts""" in {
      val integerGenerator = Gen.choose(-100, 0)
      val treesNodes       = 100
      val tree = BinaryTreeGenerator
        .treeGen(integerGenerator, treesNodes)
        .suchThat { tree =>
          !tree.right.isEmpty && !tree.left.isEmpty
        }
        .generate
      tree.analyze(0).right.value.length shouldBe 0
      tree.analyze(1).right.value.length shouldBe 1
      tree.analyze(2).right.value.length shouldBe 2
      tree.analyze(3).right.value.length shouldBe 2
      tree.analyze(4).right.value.length shouldBe 2
    }

  }

}
